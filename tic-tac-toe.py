from random import randint

game_board = []

def init():
    global game_board
    game_board = [[0, 0, 0],
                  [0, 0, 0],
                  [0, 0, 0]]

def in_range(list, game_board):
    for k in list:
        if not (k >= 0 and k < len(game_board)):
            return False
    return True

def game_over_eugene(game_board):
    for x in range(len(game_board)):
        win = 0
        check = 0

        for col in range(len(game_board)):
            row = x
            temp = game_board[row][col]
            if temp != 0:
                if check == 0:
                    check = temp
                elif check != temp:
                    break
                win += 1
            if win == len(game_board):
                return True

        win = 0
        for row in range(len(game_board)):
            col = x
            temp = game_board[row][col]
            if temp != 0:
                if check == 0:
                    check = temp
                elif check != temp:
                    break
                win += 1
            if win == len(game_board):
                return True

    win = 0
    for row in range(len(game_board)):
        win = 0
        check = 0
        temp = game_board[row][row]
        if temp != 0:
            if check == 0:
                check = temp
            elif check != temp:
                break
            win += 1
        if win == len(game_board):
            return True

    win = 0

    for row in range(len(game_board)):
        win = 0
        check = 0
        temp = game_board[row][len(game_board) - row - 1]
        if temp != 0:
            if check == 0:
                check = temp
            elif check != temp:
                break
            win += 1
        if win == len(game_board):
            return True

    return False

def game_over(game_board, who_play_with):
    empty_sell = 0
    for d in range(len(game_board)):
        for h in range(len(game_board)):
            if game_board[d][h] == 0:
                empty_sell += 1
    if empty_sell > 0:
        for k in range(len(game_board)):
            for j in range(len(game_board)):
                if game_board[k][j] != 0:
                    cell_to_check = game_board[k][j]
                    directions = [[(0, 1), (0, -1)],
                                  [(1, 0), (-1, 0)],
                                  [(-1, 1), (1, -1)],
                                  [(1, 1), (-1, -1)]]
                    for r in range(len(directions)):
                        r_list = directions[r]
                        win = 1
                        stop_current_direction = False
                        for c in range(len(r_list)):
                            if c == 1 and stop_current_direction:
                                break
                            temp_k = k
                            temp_j = j
                            combinations = r_list[c]
                            com_row = combinations[0]
                            com_col = combinations[1]
                            while in_range([temp_k + com_row, temp_j + com_col], game_board):
                                if game_board[temp_k + com_row][temp_j + com_col] == cell_to_check:
                                    win += 1
                                    temp_k += com_row
                                    temp_j += com_col
                                    if win == len(game_board):
                                        if who_play_with == "No":
                                            print("Game over. The winner is player {}.".format(1 if cell_to_check == 1 else 2))
                                        else:
                                            print("Game over. The winner is {}.".format("player 1" if cell_to_check == 1 else "computer"))
                                        return True
                                else:
                                    stop_current_direction = True
                                    win = 1
                                    break
        return False
    else:
        print("There is no winner.")
    return True

def draw_game_board(game_board):
    header = " _" * (len(game_board))
    footer = " ¯" * (len(game_board))
    symbol = ""
    print(header)
    for row in range(len(game_board)):
        for col in range(len(game_board)):
            if game_board[row][col] == 0:
                symbol = "░"
            elif game_board[row][col] == 1:
                symbol = "X"
            elif game_board[row][col] == 2:
                symbol = "Θ"
            if col == 0:
                symbol = "|{}⌠".format(symbol)
            elif col == len(game_board) - 1:
                symbol = "{}|".format(symbol)
            elif col > 0 and col < len(game_board) - 1:
                symbol = "{}⌠".format(symbol)
            print(symbol, end="")
        print("")
    print(footer)

def computer_player_ver1(game_board):
    for row in range(len(game_board)):
        for col in range(len(game_board)):
            if game_board[row][col] == 0:
                game_board[row][col] = 2
                break
        break
    return row, col

def you_shall_not_win(game_board):
    for k in range(len(game_board)):
        for j in range(len(game_board)):
            if game_board[k][j] == 1:
                print("cell_to_check = ", k, j)
                cell_to_check = game_board[k][j]
                directions = [[(0, 1), (0, -1)],
                              [(1, 0), (-1, 0)],
                              [(-1, 1), (1, -1)],
                              [(1, 1), (-1, -1)]]
                for r in range(len(directions)):
                    r_list = directions[r]
                    print("directions = ", r_list)
                    win = 1
                    continue_check = True
                    for c in range(len(r_list)):
                        print("continue_check = ", continue_check)
                        if continue_check == True:
                            temp_k = k
                            temp_j = j
                            combinations = r_list[c]
                            com_row = combinations[0]
                            com_col = combinations[1]
                            print("combinations = ", combinations)
                            print("com_row = ", combinations[0])
                            print("com_col = ", combinations[1])
                            while in_range([temp_k + com_row, temp_j + com_col], game_board):
                                print("Checking = ", temp_k + com_row, temp_j + com_col)
                                if game_board[temp_k + com_row][temp_j + com_col] == 2:
                                    continue_check = False
                                    print("It's 2, break directions")
                                    break
                                elif game_board[temp_k + com_row][temp_j + com_col] == 0:
                                    where_to_move_row = temp_k + com_row
                                    where_to_move_col = temp_j + com_col
                                    temp_k += com_row
                                    temp_j += com_col
                                    print("It's 0")
                                    print("where_to_move = ", where_to_move_row, where_to_move_col)
                                    print("new temp_k, temp_j = ", temp_k, temp_j)
                                elif game_board[temp_k + com_row][temp_j + com_col] == cell_to_check:
                                    win += 1
                                    temp_k += com_row
                                    temp_j += com_col
                                    print("It's 1. Win = ", win)
                                    print("new temp_k, temp_j = ", temp_k, temp_j)
                    if win == len(game_board) - 1:
                        print("Got win = ", win)
                        game_board[where_to_move_row][where_to_move_col] = 2
                        return where_to_move_row, where_to_move_col
    return None

def computer_player_random(game_board):
    move_not_done = True
    while move_not_done:
        move_list = [
            randint(0, len(game_board) - 1),
            randint(0, len(game_board) - 1)
        ]
        if game_board[move_list[0]][move_list[1]] == 0:
            game_board[move_list[0]][move_list[1]] = 2
            move_not_done = False
    return move_list[0], move_list[1]

init()
draw_game_board(game_board)
first_player_turn = True
who_play_with_input = False
while not who_play_with_input:
    who_play_with = input("Would you like to play with computer? Enter Yes / No.")
    if who_play_with == "Yes":
        who_play_with_input = True
        print("You are playing with computer.")
        while not game_over(game_board, who_play_with):
            if first_player_turn:
                players_input = input("Player 1: ")
                players_input_list = players_input.split(",")
                players_input_list[0] = int(players_input_list[0])
                players_input_list[1] = int(players_input_list[1])
                if in_range(players_input_list, game_board):
                    if game_board[players_input_list[0]][players_input_list[1]] == 0:
                        game_board[players_input_list[0]][players_input_list[1]] = 1 if first_player_turn else 2
                        draw_game_board(game_board)
                        first_player_turn = not first_player_turn
                    else:
                        print("Cell is not empty, try again.")
                else:
                    print("Out of range, try again.")
            else:
                you_shall_not_win_result = you_shall_not_win(game_board)
                if you_shall_not_win_result == None:
                    print("Random")
                    move = computer_player_random(game_board)
                    print("Computer: {},{}".format(move[0], move[1]))
                    draw_game_board(game_board)
                    first_player_turn = not first_player_turn
                else:
                    print("you_shall_not_win")
                    move = you_shall_not_win_result
                    print("Computer: {},{}".format(move[0], move[1]))
                    draw_game_board(game_board)
                    first_player_turn = not first_player_turn
    elif who_play_with == "No":
        who_play_with_input = True
        print("You are playing with Player 2.")
        while not game_over(game_board, who_play_with):
            players_input = input("Player {}: ".format(1 if first_player_turn else 2))
            players_input_list = players_input.split(",")
            players_input_list[0] = int(players_input_list[0])
            players_input_list[1] = int(players_input_list[1])
            if in_range(players_input_list, game_board):
                if game_board[players_input_list[0]][players_input_list[1]] == 0:
                    game_board[players_input_list[0]][players_input_list[1]] = 1 if first_player_turn else 2
                    draw_game_board(game_board)
                    first_player_turn = not first_player_turn
                else:
                    print("Cell is not empty, try again.")
            else:
                print("Out of range, try again.")
    else:
        print("Incorrect answer. Enter Yes / No.")